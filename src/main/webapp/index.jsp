<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html ng-app="onlineCounter">
    <head>
        <script src="js/angular.js"></script>
        <script src="js/angularScript.js"></script>
        <script type="text/javascript" src="js/BlogPush.js"></script>
    </head>
    <body onload="openSocket()">
        <div>
            <a href="Controller">Home</a>
            <a href="Controller?action=FriendOverview">Friends</a>
        </div>
        
	<main>
	
        <c:if test="${errors.size()>0 }">
            <div class="danger">
		<ul>
                    <c:forEach var="error" items="${errors }">
                        <li>${error }</li>
                    </c:forEach>
		</ul>
            </div>
        </c:if>
            
        <c:choose>
            <c:when test="${user!=null}">
		<p>Welcome ${user.getFirstName()}!</p>
		<form method="post" action="Controller?action=LogOut">
                    <p>
                        <input type="submit" id="logoutbutton" value="Log Out">
                    </p>
		</form>
            </c:when>
            <c:otherwise>
		<form method="post" action="Controller?action=LogIn">
                    <p>
                        <label for="email">Your UserID </label>
                        <input type="text" id="email" name="email">
                    </p>
                    <p>
                        <label for="password">Your password</label>
                        <input type="password" id="password" name="password">
                    </p>
                    <p>
                        <input type="submit" id="loginbutton" value="Log in">
                    </p>
		</form>
            </c:otherwise>
        </c:choose> 
        
        </main>

        <div id="angular" ng-controller="counterController">
            <table>
                <thead>
                        <tr>
                            <td>Status</td>
                            <td>Counter</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="(key, value) in counter">
                            <td>{{key}}</td>
                            <td>{{value}}</td>
                        </tr>                        
                    </tbody>
            </table>
        </div>
        
        <div id="HardCodedBlog" onload="openSocket()">
            <div id="Topic_A">
                <div id="Topic_A_Text">
                    <p>What is your Quest?</p>
                </div>
                <div id="Topic_A_Comments">
                </div>
                <form>
                    <label for="Topic_A_NewComment">New Comment</label>
                    <textarea id="Topic_A_NewComment" name="Topic_A_NewComment" rows="5" cols="60"></textarea>
                    <input type="button" id="Comment_To_A" value="Comment" onclick="addComment('A')"/>
                </form>
            </div>
            <div id="Topic_B">
                <div id="Topic_B_Text">
                    <p>What is the average speed of an unloaded swallow?</p>
                </div>
                <div id="Topic_B_Comments">
                </div>
                <form>
                    <label for="Topic_B_NewComment">New Comment</label>
                    <textarea id="Topic_B_NewComment" name="Topic_B_NewComment" rows="5" cols="60"></textarea>
                    <input type="button" id="Comment_To_B" value="Comment" onclick="addComment('B')"/>
                </form>
            </div>
            <div id="Topic_C">
                <div id="Topic_C_Text">
                    <p>Is it an African or European swallow?</p>
                </div>
                <div id="Topic_C_Comments">
                </div>
                <form>
                    <label for="Topic_C_NewComment">New Comment</label>
                    <textarea id="Topic_C_NewComment" name="Topic_C_NewComment" rows="5" cols="60"></textarea>
                    <input type="button" id="Comment_To_C" value="Comment" onclick="addComment('C')"/>
                </form>
            </div>
        </div>
</body>
</html>