
var socket;
var comments;
var newComment;

//pushing blogs
function addComment(topic){
    targetId = "Topic_"+topic;
    comments = document.getElementById(targetId+"_Comments");
    newComment = document.getElementById(targetId+"_NewComment").value;
    var obj = {topic:targetId, message:newComment};
    socket.send(JSON.stringify(obj));
}

function openSocket(){
    //WebSocket.CLOSED is een javascript native constante
    if(socket !== undefined && socket.readyState !== WebSocket.CLOSED){
        return;
    }
    socket = new WebSocket("ws://localhost:8080/GTW/echo");
    
    /* websocket toestanden; 
    * wordt op onopen geinitialiseerd bij het aanmaken (new)
    * WebSocket.send() triggerd een change naar onmessage
    * WebSocket.close() triggered onclose
    * 
    * elke status hangt samen met een geannoteerde methode in 
    * het ServerEndPoint
    * */
   
    //socket.onopen = function(event){};
    //socket.onclose = function(event){};
    socket.onmessage = function(event){
//        console.log(event.data);
        var data = JSON.parse(event.data);
//        console.log(data.topic);
//        console.log(data.message);
        writeComment(data.topic, data.message);
    };
}

function closeSocket(){
    socket.close();
}

function writeComment(target, text){
    document.getElementById(target+"_Comments").innerHTML += "<br/>" + text;
}