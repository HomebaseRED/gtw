/* 
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var xHRObject = new XMLHttpRequest();

//polling list
function getFriendsList(){
    xHRObject.open("GET", "Controller?action=FriendListAsync", true);
    xHRObject.onreadystatechange = getFreindslistInternal;
    xHRObject.send(null);
}

function getFreindslistInternal() {
    if (xHRObject.status == 200){
        //if the whole request has been received
        if(xHRObject.readyState == 4){
            var serverResponse = JSON.parse(xHRObject.responseText);
            var targetLocation = document.getElementById("FriendList");
            
            targetLocation.innerHTML = '';
            
            for(var i = 0; i<serverResponse.length; i++){
                var tableRow = document.createElement('tr');
                var nameElement = document.createElement('td').appendChild(document.createTextNode(serverResponse[i].firstName));
                var surnameElement = document.createElement('td').appendChild(document.createTextNode(serverResponse[i].lastName));
                var nicknameElement = document.createElement('td').appendChild(document.createTextNode(serverResponse[i].userId));
                var statusElement = document.createElement('td').appendChild(document.createTextNode(serverResponse[i].status));
                var button = document.createElement('button');
                button.setAttribute("onclick", "initiateChat('"+serverResponse[i].userId+"')");
                button.setAttribute("id", "initiateChat_"+serverResponse[i].userId);
                button.innerHTML = "Chat";
                var chatInitiation = document.createElement('td').appendChild(button);
                tableRow.appendChild(nameElement);
                tableRow.appendChild(surnameElement);
                tableRow.appendChild(nicknameElement);
                tableRow.appendChild(statusElement);
                tableRow.appendChild(chatInitiation);
                targetLocation.appendChild(tableRow);
            }
            setTimeout("getFriendsList()", 15000);
        }
    }
}
