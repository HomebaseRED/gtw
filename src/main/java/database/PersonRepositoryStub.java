package database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.Person;
import java.util.HashSet;
import java.util.Set;

public class PersonRepositoryStub implements PersonRepository {
    private Set<Person> persons = new HashSet<Person>();

    public PersonRepositoryStub () {
        Person blue = new Person("BLUE", "BLUE", "Blue", "blue");
        add(blue);
        Person red = new Person("RED", "RED", "Red", "red");
        red.addFriend(blue);
        add(red);
        Person green = new Person("GREEN", "GREEN", "Green", "green");
        green.addFriend(red);
        green.addFriend(blue);
        add(green);
    }

    public Person get(String personId){
        if(personId == null){
            throw new IllegalArgumentException("No id given");
        }
        for(Person p : this.persons){
            if(p.getUserId().equals(personId)){
                return p;
            }
        }
        return null;
    }

    public Person get(String firstName, String lastName){
        for(Person p : this.persons){
            if(p.getFirstName().equals(firstName) 
                    && p.getLastName().equals(lastName)){
                return p;
            }
        }
        return null;
    }

    public List<Person> getAll(){
        return new ArrayList<Person>(persons);	
    }

    public void add(Person person){
        if(person == null){
            throw new IllegalArgumentException("No person given");
        }
        if (persons.contains(person)) {
            throw new IllegalArgumentException("User already exists");
        }
        persons.add(person);
    }

    public void addFriend(Person target, Person subject){
        Person p = this.get(target.getUserId());
        p.addFriend(subject);
        this.update(p);
    }

    public void update(Person person){
        if(person == null){
            throw new IllegalArgumentException("No person given");
        }
        persons.remove(person);
        persons.add(person);
    }

    public void delete(String personId){
        if(personId == null){
            throw new IllegalArgumentException("No id given");
        }
        for(Person p : this.persons){
            if(p.getUserId().equals(personId)){
                this.persons.remove(p);
                break;
            }
        }
    }

    public Person getAuthenticatedUser(String email, String password) {
        Person person = get(email);
        if (person != null && person.isCorrectPassword(password)) {
            return person;
        }
        else {
            return null;
        }
    }
}
