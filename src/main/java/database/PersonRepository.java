package database;

import java.util.List;

import domain.Person;
import domain.Person;

public interface PersonRepository {

	public abstract void add(Person person);

        public abstract void addFriend(Person target, Person subject);
        
	public abstract void delete(String userId);

	public abstract Person get(String userId);

        public abstract Person get(String firstName, String lastName);
        
	public abstract List<Person> getAll();
	
	public abstract Person getAuthenticatedUser(String email, String password);

	public abstract void update(Person person);

}