package controller;

import domain.ChatService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.PersonService;
import domain.Person;

public abstract class RequestHandler {
	
    private PersonService service;
    private ChatService chatService;

    public abstract String handleRequest (HttpServletRequest request, HttpServletResponse response);

    public abstract void handleRequestAsync(HttpServletRequest request, HttpServletResponse response);

    public void setModel (PersonService service) {
            this.service = service;
    }

    public PersonService getPersonService() {
            return service;
    }
    
    public void setChatService(ChatService chatService){
            this.chatService = chatService;
    }
    
    public ChatService getChatService(){
        return chatService;
    }
}
