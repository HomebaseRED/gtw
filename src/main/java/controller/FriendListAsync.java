/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controller.RequestHandler;
import domain.PersonTypeAdapter;
import domain.Person;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author FirebaseRED
 */
public class FriendListAsync extends RequestHandler{
    
    private Gson jsonConverter;
    
    public FriendListAsync(){
        this.jsonConverter = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
//        this.jsonConverter = new GsonBuilder()
//                .registerTypeAdapter(Person.class, new PersonTypeAdapter())
//                .create();
//        this.jsonConverter = new Gson();
    }

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        Person user = (Person) request.getSession().getAttribute("user");
        String json = this.jsonConverter.toJson(user.getFriends());
        response.setContentType("text");
        try{
            response.getWriter().write(json);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }     
}
