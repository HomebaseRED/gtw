/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import com.google.gson.Gson;
import domain.ChatConversation;
import domain.ChatMessage;
import domain.Couple;
import domain.Person;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Robrecht Vanhuysse
 */
public class ChatHandlerAsync extends RequestHandler {

    private Gson jsonConverter;
    
    public ChatHandlerAsync(){
        jsonConverter = new Gson();
    }
    
    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        Person user = (Person) request.getSession().getAttribute("user");
        String jsonString = null;
        if(request.getMethod().equalsIgnoreCase("POST")){
            jsonString = (String) request.getParameter("mydata");
            ChatMessage m = this.jsonConverter.fromJson(jsonString, ChatMessage.class);
            Couple<String, String> parts = new Couple(user.getUserId(),m.getRecipient());
            super.getChatService().updateConversation(parts, m);
        }else{
            jsonString = super.getChatService().getConversation(
                    user.getUserId(),request.getParameter("target"));
            try{
                response.setContentType("application/json");
                response.getWriter().write(jsonString);
            } catch (IOException ex) {
                Logger.getLogger(ChatHandlerAsync.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
