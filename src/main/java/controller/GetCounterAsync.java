/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package controller;

import com.google.gson.Gson;
import domain.Person;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Robrecht Vanhuysse
 */
public class GetCounterAsync extends RequestHandler{

    private Gson jsonconverter;
    
    public GetCounterAsync(){
        this.jsonconverter = new Gson();
    }
    
    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        List<Person> p = super.getPersonService().getPersons();
        Map<String, Integer> counters = new HashMap<String, Integer>();
        counters.put("Online", 0);
        counters.put("Offline", 0);
        counters.put("Away", 0);
        counters.put("Other", 0);
        for(Person x : p){
            int c;
            switch(x.getStatus()){
                case "Online":
                    c = counters.get("Online");
                    c++;
                    counters.put("Online", c);
                    break;
                case "Offline":
                    c = counters.get("Offline");
                    c++;
                    counters.put("Offline", c);
                    break;
                case "Away":
                    c = counters.get("Away");
                    c++;
                    counters.put("Away", c);
                    break;
                default:
                    c = counters.get("Other");
                    c++;
                    counters.put("Other", c);
                    break;
            }
//            if(counters.containsKey(x.getStatus())){
//                int c = counters.get(x.getStatus());
//                c++;
//                counters.put(x.getStatus(), c);
//            }else{
//                counters.put(x.getStatus(), 1);
//            }
        }
        String json = this.jsonconverter.toJson(counters, HashMap.class);
        try{
            response.getWriter().write(json);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
