/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Person;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author FirebaseRED
 */
public class AddFriendAsync extends RequestHandler {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        Person target = super.getPersonService().getPerson(
                (String)request.getParameter("UserID"));
        if(target != null){
            Person user = (Person) request.getSession().getAttribute("user");
            if(user!=null){
                super.getPersonService().addFriend(user, target);
            }
        }  
    }
}
