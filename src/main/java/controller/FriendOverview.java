package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FriendOverview extends RequestHandler {
	
    @Override
    public String handleRequest(HttpServletRequest request,
                    HttpServletResponse response) {
        request.setAttribute("friendList", super.getPersonService().getPersons());
        return "friendOverview.jsp";		
    }

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
