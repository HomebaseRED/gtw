
package controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author FirebaseRED
 */

@ServerEndpoint("/echo")
public class SocketInterface {
    
    private static final Set<Session> sessions = 
            Collections.synchronizedSet(new HashSet<Session>());
    
    @OnOpen
    public void onOpen(Session session){
        System.out.println(session.getId()+"connected");
        sessions.add(session);
    }
    
    @OnMessage
    public void onMessage(String message, Session session){
        System.out.println("Message from " + session.getId() + ": " + message);
        sendToAll(message);
    }
    
    @OnClose
    public void onClose(Session session){
        System.out.println("Chat " +session.getId()+" has ended");
        sessions.remove(session);
    }
    
    private void sendToAll(String message){
        for(Session s : sessions){
            try{
                s.getBasicRemote().sendText(message);
                System.out.println("+++ message sent --- "+message+" +++");
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
