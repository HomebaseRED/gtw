package controller;

import domain.ChatService;
import domain.PersonService;

public class ControllerFactory {
	
    public RequestHandler getController(String key, PersonService model, ChatService chatService) {
        return createHandler(key, model, chatService);
    }
    
    private RequestHandler createHandler(String handlerName, PersonService model, ChatService chatService) {
        RequestHandler handler = null;
        try {
            Class<?> handlerClass = Class.forName("controller."+ handlerName);
            Object handlerObject = handlerClass.newInstance();
            handler = (RequestHandler) handlerObject;
            handler.setModel(model);
            handler.setChatService(chatService);
        } catch (Exception e) {
            throw new RuntimeException("Deze pagina bestaat niet!!!!");
        }
        return handler;
    }
}
