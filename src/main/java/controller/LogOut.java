package controller;

import domain.Person;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogOut extends RequestHandler {

	@Override
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
                Person p = (Person) session.getAttribute("user");
                p.setStatus("Offline");
                super.getPersonService().updatePersons(p);
		session.invalidate();
		return "index.jsp";
	}

    @Override
    public void handleRequestAsync(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
}
