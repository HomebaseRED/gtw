/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Objects;

/**
 *
 * @author FirebaseRED
 */
public class Couple<K, V> {
    
    private K key;
    private V value;
    
    public Couple(){}
    
    public Couple(K key, V value){
        this.key = key;
        this.value = value;
    }

    //<editor-fold desc="Getters & Setters">
    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
    //</editor-fold>

    //<editor-fold desc="hashCode, equals & toString">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.key);
        hash = 79 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Couple<?, ?> other = (Couple<?, ?>) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Couple{" + "key=" + key + ", value=" + value + '}';
    }
    //</editor-fold>
    
}
