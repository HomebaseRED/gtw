/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author Robrecht Vanhuysse
 */
public class ChatConversation {
    
    private LinkedList<ChatMessage> messages;
    private Couple<String, String> participants;

    public ChatConversation() {
    }
    
    public ChatConversation(String participantA, String participantB) {
        this.participants = new Couple(participantA, participantB);
        this.messages = new LinkedList<ChatMessage>();
    }
    
    public ChatConversation(Couple<String, String> participants){
        this.participants = participants;
        this.messages = new LinkedList<ChatMessage>();
    }

    public LinkedList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(LinkedList<ChatMessage> messages) {
        this.messages = messages;
    }

    public Couple<String, String> getParticipants() {
        return participants;
    }

    public void setParticipants(Couple<String, String> participants) {
        this.participants = participants;
    }
    
    public void addMessage(ChatMessage message){
        this.messages.add(message);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        //hash = 97 * hash + Objects.hashCode(this.messages);
        hash = 97 * hash + Objects.hashCode(this.participants);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChatConversation other = (ChatConversation) obj;
        if (!(
                (this.participants.getKey().equals(
                        other.getParticipants().getKey())
                && this.participants.getValue().equals(
                        other.participants.getValue()))
                ||
                (this.participants.getKey().equals(
                        other.getParticipants().getValue())
                && this.participants.getValue().equals(
                        other.participants.getKey()))
                ))
        {
            return false;
        }
        return true;
    }
    
    public boolean equalParticipants(Couple<String, String> participants){
    if (!(
                (this.participants.getKey().equals(
                        participants.getKey())
                && this.participants.getValue().equals(
                        participants.getValue()))
                ||
                (this.participants.getKey().equals(
                        participants.getValue())
                && this.participants.getValue().equals(
                        participants.getKey()))
                ))
        {
            return false;
        }
        return true;
    }
    
}
