/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import com.google.gson.Gson;
import database.ChatRepository;
import database.ChatRepositoryStub;

/**
 *
 * @author Robrecht Vanhuysse
 */
public class ChatService {
 
    private ChatRepository chatRepo = new ChatRepositoryStub();
    private Gson jsonConverter = new Gson();
    
    public ChatService(){}
    
    public void updateConversation(Couple<String, String> participants, 
            ChatMessage message){
        ChatConversation convo = this.contains(participants, true);
        convo.addMessage(message);
        chatRepo.update(convo);
    }
    
    /*
    looks up whether the given couple is present in archive. If so, returns the conversation.
    If not, checks second parameter whether to generate a new conversation with the couple, or 
    to return null. The generate param is optional
    */
    public ChatConversation contains(Couple<String, String> participants){
        ChatConversation result = chatRepo.get(participants);
        return result;
    }
    
    public ChatConversation contains(Couple<String, String> participants, boolean generate){
        ChatConversation result = chatRepo.get(participants);
        if(result == null && generate){
            this.addConversation(participants);
            result = chatRepo.get(participants);
        }
        return result;
    }
    
    public void addConversation(Couple<String, String> participants){
        chatRepo.add(new ChatConversation(participants));
    }

    public String getConversation(String userId, String parameter) {
        Couple<String, String> c = new Couple<String, String>(userId, parameter);
        ChatConversation convo = this.contains(c, true);
        String jsonString = this.jsonConverter.toJson(convo, ChatConversation.class);
        return jsonString;
    }
    
}
