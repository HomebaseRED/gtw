/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.HashSet;



/**
 *
 * @author Robrecht Vanhuysse
 */
public class PersonTypeAdapter extends TypeAdapter{

    @Override
    public void write(JsonWriter writer, Object t) throws IOException {
        writer.beginArray();
        HashSet<Person> set = (HashSet<Person>) t;
        for (Person p : set) {
            writer.name("firstName").value(p.getFirstName());
            writer.name("lastName").value(p.getLastName());
            writer.name("userId").value(p.getUserId());
            writer.name("status").value(p.getStatus());
            
        }
//        if(t == null || !(t instanceof HashSet<Person>)){
//            writer.nullValue();
//        }else{
//            Person p = (Person) t;
//            writer.name("firstName").value(p.getFirstName());
//            writer.name("lastName").value(p.getLastName());
//            writer.name("userId").value(p.getUserId());
//            writer.name("status").value(p.getStatus());
//        }
        writer.endArray();
    }

    @Override
    public Person read(JsonReader reader) throws IOException {
        return null;
    }
}
