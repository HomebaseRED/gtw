/*
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person {

    @Expose
    private String userId;
    private String password;
    private String salt;
    @Expose
    private String firstName;
    @Expose
    private String lastName;
    @Expose
    private String status;
    private HashSet<Person> friends;

    public Person(String userId, String password, String firstName,
                    String lastName) {
            setUserId(userId);
            setHashedPassword(password);
            setFirstName(firstName);
            setLastName(lastName);
            setStatus("Online");
            this.friends = new HashSet<Person>();
    }

    public Person(String userId, String password, String salt,
                    String firstName, String lastName) {
            setUserId(userId);
            setPassword(password);
            setSalt(salt);
            setFirstName(firstName);
            setLastName(lastName);
            setStatus("Online");
            this.friends = new HashSet<Person>();
    }

    public Person() {
    }

    public void setUserId(String userId) {
            if (userId.isEmpty()) {
                    throw new IllegalArgumentException("No id given");
            }
            String USERID_PATTERN = "[_A-Za-z0-9-\\+]+";
            Pattern p = Pattern.compile(USERID_PATTERN);
            Matcher m = p.matcher(userId);
            if (!m.matches()) {
                    throw new IllegalArgumentException("Email not valid");
            }
            this.userId = userId;
    }

    public String getUserId() {
            return userId;
    }

    public String getPassword() {
            return password;
    }

    public boolean isCorrectPassword(String password) {
            if (password.isEmpty()) {
                    throw new IllegalArgumentException("No password given");
            }
            return getPassword().equals(hashPassword(password, getSalt()));
    }

    public void setPassword(String password) {
            if (password.isEmpty()) {
                    throw new IllegalArgumentException("No password given");
            }
            this.password = password;
    }

    public void setHashedPassword(String password) {
            if (password.isEmpty()) {
                    throw new IllegalArgumentException("No password given");
            }
            this.password = hashPassword(password);
    }

    private String hashPassword(String password) {
            SecureRandom random = new SecureRandom();
            byte[] seed = random.generateSeed(20);

            String salt = new BigInteger(1, seed).toString(16);
            this.setSalt(salt);

            return hashPassword(password, salt);
    }

    private String hashPassword(String password, String seed) {
            String hashedPassword = null;
            try {
                    MessageDigest crypt = MessageDigest.getInstance("SHA-1");
                    crypt.reset();
                    crypt.update(salt.getBytes("UTF-8"));
                    crypt.update(password.getBytes("UTF-8"));
                    hashedPassword = new BigInteger(1, crypt.digest()).toString(16);
            } catch (NoSuchAlgorithmException e) {
                    throw new DomainException(e.getMessage(), e);
            } catch (UnsupportedEncodingException e) {
                    throw new DomainException(e.getMessage(), e);
            }
            return hashedPassword;
    }

    public void setSalt(String salt) {
            this.salt = salt;
    }

    public String getSalt() {
            return salt;
    }

    public String getFirstName() {
            return firstName;
    }

    public void setFirstName(String firstName) {
            if (firstName.isEmpty()) {
                    throw new IllegalArgumentException("No firstname given");
            }
            this.firstName = firstName;// firstName;

    }

    public String getLastName() {
            return lastName;
    }

    public void setLastName(String lastName) {
            if (lastName.isEmpty()) {
                    throw new IllegalArgumentException("No last name given");
            }
            this.lastName = lastName;
    }

    public String getStatus(){
        return this.status;
    }

    public void setStatus(String status){
        if(status.isEmpty()){
            throw new IllegalArgumentException("No status given.");
        }
        this.status = status;
    }

    public HashSet<Person> getFriends(){
        return this.friends;
    }

    public void addFriend(Person friend){
        this.friends.add(friend);
    }

    public void removeFriend(Person friend){
        this.friends.remove(friend);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.userId);
        hash = 37 * hash + Objects.hashCode(this.firstName);
        hash = 37 * hash + Objects.hashCode(this.lastName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        return true;
    }  
}