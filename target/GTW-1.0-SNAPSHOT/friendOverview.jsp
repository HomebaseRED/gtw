<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="js/jquery-1.12.2.js"></script>
        <script type="text/javascript" src="js/FriendsList.js"></script>
        <script type="text/javascript" src="js/ChangeStatus.js"></script>
        <script type="text/javascript" src="js/AddFriend.js"></script>
        <script type="text/javascript" src="js/AsyncCounters.js"></script>
        <script type="text/javascript" src="js/ChatCode.js"></script>
    </head>
    <body onload="getFriendsList(); getCountersAsync();">
        <div>
            <a href="Controller">Home</a>
            <a href="Controller?action=FriendOverview">Friends</a>
        </div>
        <main>
            <form>
                <p>Current status: ${user.status}</p>

                <label for="StatusInput">New Status</label>
                <input type="text" name="StatusInput" id="StatusInput"/>

                <!-- status does not dynamically change since the local version 
                of user does not get updated and there is no script to fetch it 
                from the server -->
                <input type="button" id="ChangeStatus" value="Change Status" onclick="changeStatus()"/>
            </form>   
            <table>
                <thead>
                    <tr>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>NickName</td>
                        <td>Status</td>
                        <td>Initiate Chat</td>
                    </tr>
                </thead>
                <tbody id="FriendList">
                    
                </tbody>
            </table>
                
            <div id="FriendAddition">
                <form>
                    <label for="UserID">User ID</label>
                    <input type="text" name="UserID" id="UserID"/>
                    <input type="button" id="AddButton" value="Add Friend" onclick="addFriend()"/>
                </form>          
            </div>
                
            <div id="AsyncStatus">
                <table id="AsyncStatus">
                    <thead>
                        <tr>
                            <td>Status</td>
                            <td>Counter</td>
                        </tr>
                    </thead>
                    <tbody id="AsyncStatusTable">
                        
                    </tbody>
                </table>
            </div>
                
            <div id="Chatboxes">
                
            </div>
        </main>
    </body>
</html>