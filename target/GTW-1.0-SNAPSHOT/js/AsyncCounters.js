/* 
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var counterXHRObject = new XMLHttpRequest();

function getCountersAsync(){
    counterXHRObject.open("GET", "Controller?action=GetCounterAsync", true);
    counterXHRObject.onreadystatechange = getCountersAsyncInternal;
    counterXHRObject.send(null);
}

function getCountersAsyncInternal() {
    if (counterXHRObject.status == 200){
        //if the whole request has been received
        if(counterXHRObject.readyState == 4){
            var serverResponse = JSON.parse(counterXHRObject.responseText);
            var targetLocation = document.getElementById("AsyncStatusTable");
            
            targetLocation.innerHTML = '';
            
            $.each(serverResponse, 
                function(k, v){
                    var tableRow = document.createElement('tr');
                    var keyElement = document.createElement('td').appendChild(document.createTextNode(k));
                    var valueElement = document.createElement('td').appendChild(document.createTextNode(v));
                    tableRow.appendChild(keyElement);
                    tableRow.appendChild(valueElement);
                    targetLocation.appendChild(tableRow);
                }
            );
            
            setTimeout("getCountersAsync()", 15000);
        }
    }
}