/* 
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var AddFriendObject = new XMLHttpRequest();

function addFriend(){
    // encodeURIComponent om UTF-8 te gebruiken en speciale karakters om te zetten naar code
    var UserID = encodeURIComponent(document.getElementById("UserID").value);
    document.getElementById("UserID").value = "";
    
    var information = "UserID=" + UserID;
    
    AddFriendObject.open("POST", "Controller?action=AddFriendAsync", true);
    // belangrijk dat dit gezet wordt anders kan de servlet de informatie niet interpreteren!!!
    // protocol header information
    AddFriendObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    AddFriendObject.send(information);
}
