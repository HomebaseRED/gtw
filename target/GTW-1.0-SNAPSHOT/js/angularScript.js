var app = angular.module("onlineCounter", []);

app.controller("counterController", 
    function($scope, $http){
        $http.get("Controller?action=GetCounterAsync").then(function(response){
                console.log(response.data);
                $scope.counter = response.data;
            }
        );
    }
);