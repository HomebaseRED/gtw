/* 
 * Copyright (C) 2016 Robrecht Vanhuysse 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var currentChats = [];
var timeOutId = 0;

function initiateChat(userId){
    if($.inArray(userId, currentChats) == -1){
        currentChats.push(userId);
        var window = $("<form></form>").attr({
            "id":("chatWindow_"+userId)
        });
        var heading = $("<p></p>").text("Your chat with: "+userId);
        //paste in chat history
        var history = $("<div></div>")
                .attr({
                    "id":("chatHistory_"+userId)/*,
                    "onload":("chatPolling('"+userId+"')")*/
                })
                .on('load', chatPolling(userId));
        var inputField = $("<input></input>").attr({
            "id":("chatInput_"+userId),
            "type":"text"
        });
        var sendButton = $("<input></input>").attr({
            "id":("sendButton_"+userId),
            "type":"button",
            "value":"Send",
            "onclick":("sendMessage('"+ userId +"')")
        });
        var closeButton = $("<input></input>").attr({
            "id":("closeButton_"+userId),
            "type":"button",
            "value":"Close",
            "onclick":("closeChatWindow('"+ userId +"')")
        });
        window.append(heading, history, inputField, sendButton,closeButton);
        $("#Chatboxes").append(window);
    }
}

function sendMessage(userId){
    var message = $("#chatInput_"+userId).val();
    //console.log(message);
    $("#chatInput_"+userId).val(""); 
    //$("#chatHistory_"+userId).append(message+'<br>');
    var obj = {recipient:userId, message:message};
    var jsonString = JSON.stringify(obj);
    //console.log(jsonString);
    $.ajax({
        type:"POST",
        url:"Controller?action=ChatHandlerAsync",
        data:'mydata='+jsonString
    });
}

function chatPolling(userId){
    //console.log(userId);
    timeOutId = setTimeout(function(){
    $.ajax({
        type:"GET",
        url:("Controller?action=ChatHandlerAsync&target="+userId),
        dataType:"json",
        success:function(data){
            console.log(data.messages);
            $("#chatHistory_"+userId).text('');
            $.each(data.messages,function(k){
                var hist = $("#chatHistory_"+userId);
                var m = $('<p></p>').text(data.messages[k].message);
                if(data.messages[k].recipient !== userId){
                    m.css('color','#0000ff');
                }
                hist.append(m)
            });
        },
        complete:chatPolling(userId)
    })}, 2000);
}

function closeChatWindow(userId){
    clearTimeout(timeOutId);
    $("#chatWindow_"+userId).remove();
    var id = currentChats.indexOf(userId);
    currentChats.splice(id,1);
}